import logo from "./logo.svg";
import "./App.css";
import Phone from "./Components/Phone";

function App() {
  return (
    <div className="App">
      <Phone></Phone>
    </div>
  );
}

export default App;
