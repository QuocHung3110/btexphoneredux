import React, { Component } from "react";

export default class Item extends Component {
  render() {
    let { tenSP, giaBan, hinhAnh } = this.props.phone;
    return (
      <div className="col-4 p-4">
        <div class="card border-info h-100">
          <img class="card-img-top" src={hinhAnh} alt="" />
          <div class="card-body">
            <h4 class="card-title">{tenSP}</h4>
            <p class="card-text">{giaBan}</p>
            <button
              className="btn btn-outline-info"
              onClick={() => {
                this.props.renderDetail(this.props.phone);
              }}
            >
              Chi tiet
            </button>
          </div>
        </div>
      </div>
    );
  }
}
