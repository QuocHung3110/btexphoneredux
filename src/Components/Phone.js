import React, { Component } from "react";
import { data } from "./data";
import Detail from "./Detail";
import List from "./List";

export default class Phone extends Component {
  state = {
    listPhone: data,
    detail: [],
  };
  renderDetail = (phone) => {
    let clone = phone;
    this.setState({
      detail: clone,
    });
  };
  render() {
    return (
      <div>
        <List list={this.state.listPhone} renderDetail={this.renderDetail} />
        <Detail detail={this.state.detail} />
      </div>
    );
  }
}
